{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot.loader.grub.device = "/dev/sda";

  networking.hostName = "nixos-vm"; # Define your hostname.
  networking.hostId = "e1972e63";
  # networking.wireless.enable = true;  # Enables wireless.

  # Select internationalisation properties.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    wget
    binutils
    tmux
    vim
    git
    zsh
    chromium
    feh
    termite
    nix
    xscreensaver
    xclip

    gcc

    python3
    python34Packages.pyyaml

    haskellPackages.ghc
    haskellPackages.xmonad
    haskellPackages.xmonadContrib
    haskellPackages.xmonadExtras
    haskellPackages.xmobar
    haskellPackages.yeganesh
    haskellPackages.pandoc
    trayer

  ];
  

  time.timeZone = "Europe/Berlin";

  services.openssh.enable = true;
  security.sudo.enable = true;
  security.sudo.wheelNeedsPassword = false;

  # Virtualbox specifics
  services.virtualboxGuest.enable = true;

  # XMonad setup
  services.xserver = {
    enable = true;
    layout = "us";
    windowManager = {
      xmonad.enable = true;
      xmonad.enableContribAndExtras = true;
      default = "xmonad";
    };
    desktopManager.default = "none";
    displayManager.sessionCommands = ''
      ${pkgs.feh}/bin/feh --randomize --bg-fill /home/tjger/wallpapers/ &
    '';
  };

  users.defaultUserShell = "/run/current-system/sw/bin/zsh";
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.tjger = {
    createHome = true;
    home = "/home/tjger";
    description = "Tim Jaeger";
    extraGroups = [ "wheel" ];
    useDefaultShell = true;
    uid = 1000;
  };


}
