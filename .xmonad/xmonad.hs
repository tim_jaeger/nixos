import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.Fullscreen
import XMonad.Layout.NoBorders
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig

import System.IO

main :: IO ()
main = do
    xmproc <- spawnPipe "xmobar"
    xmonad $ config' xmproc

config' p = defaultConfig
  -- { terminal    = "termite -e \"tmux attach-session -t main\""
  { terminal    = "termite"
  , modMask     = mod1Mask
  , borderWidth = 0
  , workspaces  = workspaces'
  , manageHook  = manageHook'
  , layoutHook  = layouts'
  , logHook     = dynamicLogWithPP $ xmobar' p
  } `additionalKeysP` keyBindings'

workspaces' = map show [1..8]

manageHook' = composeAll
  [ isFullscreen --> doFullFloat
  -- , resource =? "stalonetray" --> doIgnore
  -- , resource =? "trayer" --> doIgnore
  , manageDocks
  ]

layouts' =
  (avoidStruts $ Tall 1 (3/100) (1/2)) |||
  (noBorders $ fullscreenFull Full)

xmobar' p = xmobarPP
  { ppOutput = hPutStrLn p
  , ppTitle  = xmobarColor "green" "" . shorten 100
  }

keyBindings' = [ ("M-p", spawn "yeganesh -x -- -fn \"Droid Sans Mono-10:normal\" | sh")
               , ("M-b", spawn "chromium --disable-hang-monitor --disable-gpu")
               -- , ("M-f", spawn "thunar")
               -- , ("M-S-b", spawn "blueman-manager")
               -- , ("M1-t", spawn "termite")
               ]
